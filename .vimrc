let tmp = system('ruby -rrbconfig -e "print RbConfig::CONFIG[%(rubyhdrdir)]"')
if v:shell_error == 0 && !empty(tmp)
  exe 'auto BufNew,BufRead ext/*.c setl path+=' . tmp
end
unlet tmp
