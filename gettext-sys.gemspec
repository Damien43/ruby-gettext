File.read('lib/gettext.rb') =~ /VERSION\s*=\s*['"]([^"']+)/
version = $1

exclude = [
  /(^|\/)\.\w+/, # Dotfiles
]

Gem::Specification.new do |spec|
  spec.name          = 'gettext-sys'
  spec.version       = version
  spec.authors       = ['Damien']
  spec.summary       = 'Simple binding for GNU gettext library'
  spec.license       = 'GPL'
  spec.files         = %w(ext/gettext.c
                          lib/gettext.rb)
  spec.extensions    = ['ext/extconf.rb']
end
