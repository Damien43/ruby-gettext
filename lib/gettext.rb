# This module export only the *gettext functions family,
# setlocale() anc LC_* constants.
# For more information see the related man pages.

module Gettext
  VERSION = '0.1.0'
  extend self
end

require 'gettext.so'
