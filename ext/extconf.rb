require 'mkmf.rb'

have_header 'libintl.h'
%w(gettext
   dgettext
   ngettext
   dcngettext
   dngettext
   textdomain
   bindtextdomain
   bind_textdomain_codeset
).all? {|f| have_func f } or exit 1

create_makefile 'gettext'
