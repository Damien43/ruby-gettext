#include <ruby.h>
#include <libintl.h>
#include <locale.h>

static VALUE mGettext;

#define self VALUE self __attribute__((unused))

/*
 * call-seq: gettext(msgid)
 */
static VALUE
gettext_(self, VALUE msgid)
{
    char *m = StringValueCStr(msgid);
    return rb_str_new_cstr(gettext(m));
}

/*
 * call-seq: dcgettext(domainname, msgid, category)
 */
static VALUE
dcgettext_(self, VALUE domainname, VALUE msgid, VALUE category)
{
    char *m = StringValueCStr(msgid);
    char *d = StringValueCStr(domainname);
    Check_Type(category, T_FIXNUM);
    return rb_str_new_cstr(dcgettext(d, m, FIX2INT(category)));
}

/*
 * call-seq: dgettext(domainname, msgid)
 */
static VALUE
dgettext_(self, VALUE domainname, VALUE msgid)
{
    char *m = StringValueCStr(msgid);
    char *d = StringValueCStr(domainname);
    return rb_str_new_cstr(dgettext(d, m));
}

/*
 * call-seq: ngettext(msgid, msgid_plural, count)
 */
static VALUE
ngettext_(self, VALUE msgid, VALUE msgid_plural, VALUE n)
{
    char *m  = StringValueCStr(msgid);
    char *ms = StringValueCStr(msgid_plural);
    Check_Type(n, T_FIXNUM);
    return rb_str_new_cstr(ngettext(m, ms, FIX2ULONG(n)));
}

/*
 * call-seq: dcngettext(domainname, msgid, msgid_plural, count, category)
 */
static VALUE
dcngettext_(self, VALUE domainname, VALUE msgid, VALUE msgid_plural, VALUE n, VALUE category)
{
    char *d  = StringValueCStr(domainname);
    char *m  = StringValueCStr(msgid);
    char *ms = StringValueCStr(msgid_plural);
    Check_Type(n, T_FIXNUM);
    Check_Type(category, T_FIXNUM);
    return rb_str_new_cstr(dcngettext(d, m, ms, FIX2ULONG(n), FIX2INT(category)));
}

/*
 * call-seq: dngettext(domainname, msgid, msgid_plural, count)
 */
static VALUE
dngettext_(self, VALUE domainname, VALUE msgid, VALUE msgid_plural, VALUE n)
{
    char *d  = StringValueCStr(domainname);
    char *m  = StringValueCStr(msgid);
    char *ms = StringValueCStr(msgid_plural);
    Check_Type(n, T_FIXNUM);
    return rb_str_new_cstr(dngettext(d, m, ms, FIX2ULONG(n)));
}

/*
 * call-seq: textdomain(domain = nil)
 */
static VALUE
textdomain_(int argc, VALUE *argv, self)
{
    char *domain = NULL;
    if (argc > 1)
	rb_raise(rb_eArgError, "Too many arguments (%d for 0..1)", argc);
    else if (argc)
	domain = StringValueCStr(*argv);
    return rb_str_new_cstr(textdomain(domain));
}

/*
 * call-seq: bindtextdomain(domain, dirname = nil)
 */
static VALUE
bindtextdomain_(int argc, VALUE *argv, self)
{
    char *dirname = NULL;
    char *domain;
    if (argc == 0 || argc > 2)
	rb_raise(rb_eArgError, "Usage: bindtextdomain(domain, dirname = nil)");
    else if (argc == 2 && RTEST(argv[1]))
	dirname = StringValueCStr(argv[1]);
    domain = StringValueCStr(argv[0]);
    return rb_str_new_cstr(bindtextdomain(domain, dirname));
}

/*
 * call-seq: bind_textdomain_codeset(domaine, codeset = nil)
 */
static VALUE
bind_textdomain_codeset_(int argc, VALUE *argv, self)
{
    char *code = NULL;
    char *domain;
    if (argc == 0 || argc > 2)
	rb_raise(rb_eArgError, "Usage: bind_textdomain_codeset(domain, codeset = nil)");
    else if (argc == 2 && RTEST(argv[1]))
	code = StringValueCStr(argv[1]);
    domain = StringValueCStr(argv[0]);
    return rb_str_new_cstr(bindtextdomain(domain, code));
}

/*
 * call-seq: setlocale(category, locale = nil)
 */
static VALUE
setlocale_(int argc, VALUE *argv, self)
{
    char *l = NULL;
    if (argc == 0 || argc > 2)
	rb_raise(rb_eArgError, "Usage: setlocale(category, locale = nil)");
    else if (argc == 2 && RTEST(argv[1]))
	l = StringValueCStr(argv[1]);
    Check_Type(*argv, T_FIXNUM);
    l = setlocale(FIX2INT(argv[0]), l);
    return l ? rb_str_new_cstr(l) : Qnil; /* Or throw a exception? */
}

void
Init_gettext(void)
{
    mGettext = rb_define_module("Gettext");
    rb_define_const(mGettext, "LC_CTYPE", INT2FIX(LC_CTYPE));
    rb_define_const(mGettext, "LC_NUMERIC", INT2FIX(LC_NUMERIC));
    rb_define_const(mGettext, "LC_COLLATE", INT2FIX(LC_COLLATE));
    rb_define_const(mGettext, "LC_MONETARY", INT2FIX(LC_MONETARY));
    rb_define_const(mGettext, "LC_MESSAGES", INT2FIX(LC_MESSAGES));
    rb_define_const(mGettext, "LC_ALL", INT2FIX(LC_ALL));

    rb_define_method(mGettext, "gettext", gettext_, 1);
    rb_define_method(mGettext, "dcgettext", dcgettext_, 3);
    rb_define_method(mGettext, "dgettext", dgettext_, 2);
    rb_define_method(mGettext, "ngettext", ngettext_, 3);
    rb_define_method(mGettext, "dcngettext", dcngettext_, 5);
    rb_define_method(mGettext, "dngettext", dngettext_, 4);
    rb_define_method(mGettext, "textdomain", textdomain_, -1);
    rb_define_method(mGettext, "bindtextdomain", bindtextdomain_, -1);
    rb_define_method(mGettext, "bind_textdomain_codeset", bind_textdomain_codeset_, -1);
    rb_define_method(mGettext, "setlocale", setlocale_, -1);
}
